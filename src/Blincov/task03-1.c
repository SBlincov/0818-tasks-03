//
//  main.c
//  Task03-1
//
//  Created by Blincov Sergey on 30.05.16.
//  Copyright © 2016 Blincov Sergey. All rights reserved.
//

#include <stdio.h>
#include <string.h>

int main(){
	char str[3][5];
	int i=0; unsigned long length = 0;
	printf("Enter strings \n");
	while ((i < 3) && (length != 0)){
		fgets(str[i], 5, stdin);
		str[i][strlen(str[i]) - 1] = '\0';
		length = strlen(str[i]);
		i++;
		}
	printf("Result: \n");
	for (int i = 0; i >=0; i--){
		for (unsigned long j = strlen(str[i]); j >= 0; j--){
			printf("%c", str[i][j]);
		}
	}
	return 0;
}
