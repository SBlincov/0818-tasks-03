//
//  main.c
//  Task03-3
//
//  Created by Blincov Sergey on 29.05.16.
//  Copyright © 2016 Blincov Sergey. All rights reserved.
//

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main()
{
	char arr[5][256];
	int i=0,j=0,str;
	puts("Enter your strings");
	while (i<5){
		fgets(arr[i], 256, stdin);
		if (arr[i][0]=='\n')
			break;
		i++;
		j++;
	}
	str = j;
	for (i=1;i<=256;i++)
		for (j=0;j<str;j++)
			if (strlen(arr[j]) == i)
				printf("%s", arr[j]);
	return 0;
}