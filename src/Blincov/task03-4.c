//
//  main.c
//  task03-4
//
//  Created by Blincov Sergey on 30.05.16.
//  Copyright © 2016 Blincov Sergey. All rights reserved.
//
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

int main(){
	char str[5][256];
	int i=0,j=0,random[5],temp,a;
	puts("Enter your strings");
	while (i<5){
		fgets(str[i], 256, stdin);
		if (str[i][0]=='\n')
			break;
		i++;
		j++;
	}
	srand(time(0));
	printf("\n");
	for (i=0; i<j; i++)
		random[i]=i;
	for (i=0; i<j; i++){
		a=rand()%j;
		temp=random[i];
		random[i]=random[a];
		random[a]=temp;
	}
	for(i=0;i<j;i++)
		printf("%s", str[random[i]]);
	return 0;
}